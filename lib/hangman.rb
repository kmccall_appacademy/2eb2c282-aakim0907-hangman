class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players, guess = 7)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @guess_count = guess
  end

  def play_game
    puts
    puts "***Let's play <Hangman>!!!***"
    setup
    until @guess_count == 0
      take_turn
      won? ? conclude : next
    end
    conclude
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def display_board
    puts @board.map { |ele| ele == nil ? "_" : ele }.join
    # puts "So far you've guessed, #{@guesser.guessed_letters}"
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    display_board
    @guesser.handle_response(guess, indices)
    @guess_count -= 1
  end

  def length
    @board.length
  end

  def won?
    @board.join == @referee.secret_word
  end

  def conclude
    puts "***End of the game!***"
    puts "The word was [#{@referee.secret_word.chomp}]"
  end

  private

  def update_board(idx_arr, guess)
    idx_arr.each { |idx| @board[idx] = guess }
  end
end

class HumanPlayer
  attr_reader :name, :guessed_letters, :secret_word

  def initialize(name = "Alex")
    @name = name
    @guessed_letters = []
  end

  def pick_secret_word
    puts "*Enter the length of your secret word!"
    @secret_word = gets.chomp
    @secret_word.length
  end

  def register_secret_length(length)
    puts "(Length of the secret word: #{length})"
  end

  def guess
    puts "*Make a guess!"
    print "> "
    guess = gets.chomp
    manage_guess(guess)
  end

  def check_guess(guess)
    puts "*The computer guessed the letter #{guess}."
    puts "*Enter the indices for the location of this letter. (ex. 1 2 4)"
    puts "(If the letter is not included, just hit enter)"
    gets.chomp.split.map { |ele| ele.to_i - 1 }
  end

  def handle_response(letter, indices)
    puts
  end

  private

  def manage_guess(guess)
    unless @guessed_letters.include?(guess)
      @guessed_letters << guess
      return guess
    else
      puts "*You already guessed #{guess}. Please guess again."
      puts "*So far you've guessed, #{@guessed_letters}"
      print "> "
      another_guess = gets.chomp
      manage_guess(another_guess)
    end
  end
end

class ComputerPlayer
  attr_reader :secret_word

  def initialize(dict = File.readlines("dictionary.txt"))
    @dictionary = dict
    @alphabets = ("a".."z").to_a
  end

  def candidate_words
    @dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @dictionary.select! { |ele| ele.length == length}
    @secret_word = @dictionary.sample
  end

  def guess(board)
    abc_hash = Hash.new(0)
    @dictionary.each do |word|
      word.chars.each { |ch| abc_hash[ch] += 1 }
    end
    sorted_abc = abc_hash.sort_by { |_, v| v }
    guess = sorted_abc.pop
    guess[0]
  end

  def check_guess(guess)
    (0...@secret_word.length).select { |i| @secret_word[i] == guess }
  end

  def handle_response(letter, indices)
    @dictionary.select! do |word|
      word_indices = []
      word.chars.each_with_index do |ch, idx|
        word_indices << idx if ch == letter
      end
      indices == word_indices
    end
  end
end

# if __FILE__ == $PROGRAM_NAME
#   puts "*What's your name?"
#   name = gets.chomp
#   players = {guesser: ComputerPlayer.new,
#     referee: HumanPlayer.new(name)}
#
#   game = Hangman.new(players)
#   game.play_game
# end
